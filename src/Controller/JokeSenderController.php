<?php

namespace App\Controller;

use App\Service\JokesProvider;
use App\Service\JokesSaver;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class JokeSenderController extends AbstractController
{
    /**
     * @Route("/", name="jokeSender")
     */
    public function sendJoke(Request $request, JokesProvider $jokesProvider, JokesSaver $jokesSaver, \Swift_Mailer $mailer)
    {
        $jokeCategories = $jokesProvider->getCategories();

        $defaultData = [];
        $form = $this->createFormBuilder($defaultData)
            ->add('category', ChoiceType::class, [
                'choices' => array_flip($jokeCategories)
            ])
            ->add('email', EmailType::class, [
                'required' => true
            ])
            ->add('send', SubmitType::class, [
                'label' => 'Send me a joke!'
            ])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $category = $jokeCategories[$data['category']];
            $joke = $jokesProvider->getRandomJokeFromCategory($category);

            // Send email with joke
            $subject = "Случайная шутка из {$category}";
            $message = (new \Swift_Message($subject))
                ->setFrom('noreply@jokesSender.com')
                ->setTo($data['email'])
                ->setBody($joke, 'text/plain')
            ;
            $recipients = $mailer->send($message);
            if ($recipients > 0) {
                $this->addFlash('jokeSent', "You received new joke! Let's check your e-mail: {$data['email']}");
            }
            else {
                $this->addFlash('jokeSent', "Whooops! Something wrong with your e-mail! :(");
            }

            // Save joke to file
            $jokesSaver->save($joke);

            return $this->redirectToRoute('jokeSender');
        }
        
        return $this->render('jokesSender/base.html.twig', [
            'jokeSenderForm' => $form->createView()
        ]);
    }
}