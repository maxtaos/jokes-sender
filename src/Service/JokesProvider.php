<?php

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class JokesProvider
 * Connects to ICNDb.com API and return jokes categories and random joke from one category.
 * Uses GuzzleHttp.
 * @package App\Service
 */
class JokesProvider
{
    private $httpClient;

    public function __construct()
    {
        $this->httpClient = new Client();
    }

    public function getCategories()
    {
        $categories = [];

        try {
            $res = $this->httpClient->request('GET', "http://api.icndb.com/categories");
            $jsonValue = json_decode($res->getBody());
            $categories = $jsonValue->value;
        }
        catch (GuzzleException $exception) {

        }

        return $categories;
    }

    public function getRandomJokeFromCategory(String $category)
    {
        $joke = '';

        try {
            $res = $this->httpClient->request('GET', "http://api.icndb.com/jokes/random?limitTo=[{$category}]");
            $jsonValue = json_decode($res->getBody());
            $joke = $jsonValue->value->joke;
        }
        catch (GuzzleException $exception) {

        }

        return $joke;
    }
}