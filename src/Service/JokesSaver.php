<?php

namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;

/**
 * Class JokesSaver
 * Save a joke to file in %kernel.project_dir/var/tmp/jokes.log%.
 * @package App\Service
 */
class JokesSaver
{
    private $directoryPath = '';
    private $fileName = 'jokes.log';
    private $fileSystem;

    public function __construct(Filesystem $filesystem, String $projectDirPath)
    {
        $this->changeDirectoryPath("{$projectDirPath}/var/tmp/");

        $this->fileSystem = $filesystem;
    }

    public function changeDirectoryPath(String $newPath)
    {
        $this->directoryPath = $newPath;
    }

    public function getDirectoryPath()
    {
        return $this->directoryPath;
    }

    public function getFullPathToFile()
    {
        return "{$this->directoryPath}/{$this->fileName}";
    }

    public function save(String $joke)
    {
        if (!$this->fileSystem->exists($this->directoryPath)) {
            $this->fileSystem->mkdir($this->directoryPath);
        }
        $this->fileSystem->appendToFile($this->getFullPathToFile(), "{$joke}\n");
    }
}