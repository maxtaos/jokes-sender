<?php

namespace App\Tests\Service;

use App\Service\JokesProvider;
use PHPUnit\Framework\TestCase;

class JokesProviderTest extends TestCase
{
    public function testGetCategories()
    {
        $jokesProvider = new JokesProvider();

        $categories = $jokesProvider->getCategories();

        $this->assertGreaterThan(0, count($categories));
    }

    public function testGetRandomJokeFromCategory()
    {
        $jokesProvider = new JokesProvider();

        $categories = $jokesProvider->getCategories();
        $randomCategory = $categories[array_rand($categories)];

        $randomJoke = $jokesProvider->getRandomJokeFromCategory($randomCategory);

        $this->assertNotEmpty($randomJoke);
    }
}
