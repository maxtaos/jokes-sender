<?php

namespace App\Tests\Service;

use App\Kernel;
use App\Service\JokesSaver;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Filesystem;

class JokesSaverTest extends TestCase
{
    public function testSave()
    {
        // Create kernel, filesystem component and get project dir path
        $kernel = new Kernel('test', true);
        $filesystem = new Filesystem();

        // Create JokesSaver
        $jokesSaver = new JokesSaver($filesystem, $kernel->getProjectDir());

        // Clear current content
        $filesystem->remove($jokesSaver->getFullPathToFile());

        // Try to save a test joke and check
        $jokesSaver->save('Test joke');
        $this->assertContains("Test joke\n", file_get_contents($jokesSaver->getFullPathToFile()));

        $filesystem->remove($jokesSaver->getFullPathToFile());
    }
}
